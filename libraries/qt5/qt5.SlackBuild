#!/bin/sh

# Copyright 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Merged some ideas from Alex Sarmardzic's script for qt4 at SlackBuilds.org
# ^^ Modified by Robby Workman <rworkman@slackware.com> for QT4 & KDE4

# We're using qt-copy instead of a released qt version, as this git release
# has at least one bugfix strongly recommended by the kde developers.

# Obtained from:
# git clone git://gitorious.org/+kde-developers/qt/kde-qt.git
# git checkout origin/4.5.3-patched
# git checkout origin/4.6.0-stable-patched
# git checkout origin/4.6.1-patched
# git checkout origin/4.7.0-patched
#
# Alternate method (we don't use this):
# wget http://qt.gitorious.org/qt/kde-qt/archive-tarball/4.6.2-patched
#
# Modifications 2010, 2011, 2012  Eric Hameleers, Eindhoven, NL
# qt 4.7.3, 4.7.4, 4.8.0, 4.8.1, 4.8.2, 4.8.4 are built from original nokia sources.
#
# More Mods 2013 Michael James, AU
# Qt 5.1.0

# Copright 2013-2014 Marcel Saegebarth <marc@mos6581.de>
# Adapted from all above and
# http://wildwizard.abnormalpenguin.com/linux/slackware/qt5/.
#
# Symlink with postfix '-qt5'. Same behaviour as it were for QT3 and QT4 in the
# past from slackbuilds.org.

PRGNAM=qt5
VERSION=${VERSION:-5.1.1}
BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}

NUMJOBS=${NUMJOBS:--j7}

MARCH=$( uname -m )
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "armv7hl" ]; then
  # To prevent "qatomic_armv6.h error: output number 2 not directly addressable"
  # More permanent solution is to patch gcc:
  # http://bazaar.launchpad.net/~linaro-toolchain-dev/gcc-linaro/4.6/revision/106731
  SLKCFLAGS="-O2 -march=armv7-a -mfpu=vfpv3-d16 -fno-strict-volatile-bitfields"
  LIBDIRSUFFIX=""
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

case "$ARCH" in
    arm*) TARGET=$ARCH-slackware-linux-gnueabi ;;
    *)    TARGET=$ARCH-slackware-linux ;;
esac

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf qt-everywhere-opensource-src-$VERSION
tar xvf $CWD/qt-everywhere-opensource-src-$VERSION.tar.xz     # For qt releases
cd qt-everywhere-opensource-src-$VERSION || exit 1

chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
OPENSOURCE_CXXFLAGS="$SLKCFLAGS" \
./configure \
  -release \
  -confirm-license \
  -opensource \
  -shared \
  -prefix /usr/lib${LIBDIRSUFFIX}/qt5 \
  -system-libpng \
  -system-libjpeg \
  -system-zlib \
  -system-sqlite \
  -plugin-sql-sqlite \
  -dbus \
  -nomake examples \
  -no-separate-debug-info \
  -strip \
  -no-pch
  # No-precompiled-headers is ccache-friendly.

# Sometimes a failure happens when parallelizing make.  Try again if make fails,
# but make a failure the second time around (single threaded) a fatal error:
make $NUMJOBS || make || exit 1
make install INSTALL_ROOT=$PKG || exit 1

mkdir -p $PKG/usr/bin
( cd $PKG/usr/bin
  for file in assistant lrelease qcollectiongenerator qdbusxml2cpp qmake \
    qmlplugindump qmlviewer xmlpatterns designer lupdate qdbus qdoc \
    qml1plugindump qmlprofiler rcc xmlpatternsvalidator lconvert moc \
    qdbuscpp2xml qhelpconverter qmlbundle qmlscene linguist pixeltool \
    qdbusviewer qhelpgenerator qmlmin qmltestrunner uic ; do
    ln -sf /usr/lib${LIBDIRSUFFIX}/qt5/bin/$file ${file}-qt5
  done
)

# Add qtchooser config file
mkdir -p $PKG/etc/xdg/qtchooser
cat <<EOF > $PKG/etc/xdg/qtchooser/5.conf
/usr/lib${LIBDIRSUFFIX}/qt5/bin
/usr/lib${LIBDIRSUFFIX}
EOF
( cd $PKG/etc/xdg/qtchooser/
  ln -s 5.conf 5.1.conf
  ln -s 5.conf 5.1.1.conf
)

# Add menu entries for all those hidden but great Qt applications:
# Qt logo:
mkdir -p $PKG/usr/share/icons/hicolor/48x48/apps
convert qtdoc/doc/src/images/qt-logo.png  -resize 48x48 $PKG/usr/share/icons/hicolor/48x48/apps/qt-logo.png
# Assistant icons
install -p -m644 -D qttools/src/assistant/assistant/images/assistant.png $PKG/usr/share/icons/hicolor/32x32/apps/assistant.png
install -p -m644 -D qttools/src/assistant/assistant/images/assistant-128.png $PKG/usr/share/icons/hicolor/128x128/apps/assistant.png
# Designer icon
install -p -m644 -D qttools/src/designer/src/designer/images/designer.png $PKG/usr/share/icons/hicolor/128x128/apps/designer.png
# Linguist icons
for icon in qttools/src/linguist/linguist/images/icons/linguist-*-32.png ; do
  size=$(echo $(basename ${icon}) | cut -d- -f2)
  install -p -m644 -D ${icon} $PKG/usr/share/icons/hicolor/${size}x${size}/apps/linguist.png
done

# And the .desktop files
mkdir -p $PKG/usr/share/applications
cat <<EOF > $PKG/usr/share/applications/qt5-designer.desktop
[Desktop Entry]
Name=Qt5 Designer
GenericName=Interface Designer
Comment=Design GUIs for Qt5 applications
Exec=designer -qt=5
Icon=designer
MimeType=application/x-designer;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF
cat <<EOF > $PKG/usr/share/applications/qt5-assistant.desktop
[Desktop Entry]
Name=Qt5 Assistant
Comment=Shows Qt5 documentation and examples
Exec=assistant -qt=5
Icon=assistant
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;Documentation;
EOF
cat <<EOF > $PKG/usr/share/applications/qt5-linguist.desktop
[Desktop Entry]
Name=Qt5 Linguist
Comment=Add translations to Qt5 applications
Exec=linguist -qt=5
Icon=linguist
MimeType=text/vnd.trolltech.linguist;application/x-linguist;
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Development;
EOF
cat <<EOF > $PKG/usr/share/applications/qt5-config.desktop
[Desktop Entry]
Name=Qt5 Config
Comment=Configure Qt5 behavior, styles, fonts
Exec=qtconfig -qt=5
Icon=qt-logo
Terminal=false
Encoding=UTF-8
Type=Application
Categories=Qt;Settings;
EOF

# Add a documentation directory:
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a LGPL_EXCEPTION.txt LICENSE* README* \
  $PKG/usr/doc/$PRGNAM-$VERSION
if [ -d $PKG/usr/lib${LIBDIRSUFFIX}/qt/doc/html ]; then
  ( cd $PKG/usr/doc/$PRGNAM-$VERSION
    ln -sf /usr/lib${LIBDIRSUFFIX}/qt/doc/html .
  )
fi

mkdir -p $PKG/install
cat $CWD/doinst.sh > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.txz
