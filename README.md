# marc-slackbuilds

2015-09-23: Official SlackBuilds now moved to the slackbuilds.org git repository

SlackBuilds maintained, but not all, for slackbuilds.org. Some are discontinued or just adapted from other maintainers from slackbuilds.org. Whilst they are written for the stable Slackware releases (as slackbuilds.org doesn't support Slackware current) all should build fine on current either and normally also on multilib systems.
