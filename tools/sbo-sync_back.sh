#!/bin/sh

# Copyright 2015 Marcel Saegebarth <marc@mos6581.de>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

declare -A PACKAGES REPO

REPOLOCAL=/home/marc/projects/git/marc-slackbuilds/*
REPOSBO=/home/marc/projects/git/swmarc-slackbuilds.org/*

DESTINATION=${1:-sbo}

. ./sbo-sync_back.blacklist

BOLD=$(tput bold)
NORMAL=$(tput sgr0)
GREEN=$(tput setf 2)
YELLOW=$(tput setf 6)

for SBO in $(find $REPOSBO -type d); do
  PACKAGES["$(basename $SBO)"]="$SBO"
done

LENGTH=0
LENGTHNAM=''
for LOCAL in $(find $REPOLOCAL -type d); do
  BASENAME=$(basename $LOCAL)
  REPO["$BASENAME"]="$LOCAL"
  if [ ${#BASENAME} -gt $LENGTH ]; then
    LENGTH=${#BASENAME}
    LENGTHNAM=${BASENAME}
  fi
done

msg_type () {
  case "$1" in
    1)
      TYPE="*** Skipping"
      COLOR=${YELLOW}
    ;;
    2)
      TYPE="+++ Syncing "
      COLOR=${GREEN}
    ;;
  esac

  BASENAME=$2
  STATUS=$3
  WIDTHMAX="${BOLD}${COLOR}${TYPE}${NORMAL} ${BOLD}\`$LENGTHNAM'${NORMAL}:"

  printf "\n%-${#WIDTHMAX}s %s" "${BOLD}${COLOR}${TYPE}${NORMAL} ${BOLD}\`$BASENAME'${NORMAL}:" "${STATUS}"
}

# set permissions
find -L ${REPOLOCAL} -type f -exec chmod 644 {} \;
find -L ${REPOLOCAL} -type d -exec chmod 755 {} \;

for LOCAL in $REPOLOCAL/*; do
  BASENAME=$(basename "${LOCAL}")
  
  # skip SlackBuilds that I have, but not available at SBo
  #if test "${EXCEPTIONS[$BASENAME]+isset}"; then
  #  continue
  #fi

  # copy over packages from local to SBo repository or vice versa
  if test "${PACKAGES[$BASENAME]+isset}"; then

    # check for local .info file
    if [ ! -f "${LOCAL[$BASENAME]}/$BASENAME.info" ]; then
      continue
    fi

    # check for remote .info file
    if [ ! -f "${PACKAGES[$BASENAME]}/$BASENAME.info" ]; then
      continue
    fi

    # compare maintainer and if not equal skip
    . ${LOCAL[$BASENAME]}/$BASENAME.info
    MAINTAINER_LOCAL=$MAINTAINER
    . ${PACKAGES[$BASENAME]}/$BASENAME.info
    MAINTAINER_REMOTE=$MAINTAINER

    # skip SlackBuilds that I have, but I don't maintain on SBo
    if [ "$MAINTAINER_LOCAL" != "$MAINTAINER_REMOTE" ]; then
      continue
    fi

    if [ "${DESTINATION}" = "local" ]; then
      FROM="${PACKAGES[$BASENAME]}/*"
      TO="${LOCAL[$BASENAME]}"
    else
      FROM="${LOCAL[$BASENAME]}/*"
      TO="${PACKAGES[$BASENAME]}"
    fi

    if [ $(rsync -va ${FROM} ${TO} | wc -l) -gt 4 ]; then
      msg_type 2 ${BASENAME} "${BOLD}${GREEN}OK${NORMAL}"
    else
      msg_type 1 ${BASENAME} "${BOLD}${GREEN}Identical${NORMAL}"
    fi
  fi
done

echo ${NORMAL}
